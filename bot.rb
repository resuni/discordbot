require('discordrb')
require('json')
require('net/http')
require('streamio-ffmpeg')
require('typhoeus')

require_relative('reddit.rb')

if ARGV.empty? or ARGV[0] == 0
  puts "Usage: ruby bot.rb <token>"
  exit
end

# Instantiate Bot object.
bot = Discordrb::Bot.new token: ARGV[0]

bot.message do |event|

  # Define triggers and associated actions in case statement.
  case event.message.content
    when /cough/i
      event.respond("THIS MUTHAFUKA GOT THE RONA")
    when /https:\/\/redd\.it\/(.*)/i
      Reddit.new(event, $1, nil)
    when /https:\/\/(www\.)?reddit.com\/r\/.*?\/comments\/(?<fullname>.*?)\/.*?\/((?<comment>.*?)\/)?/i
      Reddit.new(event, $~[:fullname], $~[:comment])
  end

end

bot.run
