class Reddit

  def initialize (event, fullname, comment)

    # Return if a comment is linked to.
    if comment != nil
      return
    end

    # Use reddit API to download information about the post.
    ty_reddit = Typhoeus.get("https://www.reddit.com/api/info.json?id=t3_#{fullname}")
    if ty_reddit.code == 200
      rObject = JSON.parse(ty_reddit.body)
    else
      return "Error fetching post information: " + ty_reddit.code
    end

    # Determine post type.
    case rObject["data"]["children"][0]["data"]["url"]
      when /gif$|gifv$|^https:\/\/gfycat\.com\/\w+$/i
        event.respond(rObject["data"]["children"][0]["data"]["url"])
      when /^https:\/\/v\.redd\.it\//i
        self.vreddit(event, fullname, rObject)
    end
  
  end

  # Function to handle encoding reddit-hosted videos.
  def vreddit (event, fullname, rObject)
  
    # Encode mp4 file from DASH manifest via URL found in reddit API response.
    manifestURL = rObject["data"]["children"][0]["data"]["secure_media"]["reddit_video"]["dash_url"]
    movie = FFMPEG::Movie.new(manifestURL)
    outfile = "tmp/#{fullname}.mp4"
    movie.transcode(outfile)
  
    # Upload file.
    event.send_file(File.open(outfile, 'r'))
  
    # Delete file.
    File.delete(outfile)
  
  end

end
