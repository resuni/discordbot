FROM ruby

WORKDIR /usr/src/app
COPY . ./
RUN mkdir tmp

RUN apt-get update
RUN apt-get install -y ffmpeg libopus0 libsodium23 libsodium-dev
RUN bundle install

ENV SECRET 0

CMD ruby bot.rb $SECRET
